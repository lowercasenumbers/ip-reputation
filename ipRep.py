#!/usr/bin/ python
import config
import requests
from requests.exceptions import HTTPError
import json
import sys
import argparse

def ipQualityScore(ip):
	print('Checking the Reputation of '+ str(ip))
	baseUrl = 'https://www.ipqualityscore.com/api/json/ip/'+str(ipQualityAPI)
	apiUrl = baseUrl + '/' + str(ip) + '?strictness=0&allow_put'
	#Check the IP for Reputation
	try:
		response = requests.post(apiUrl)
		response.raise_for_status()
	except HTTPError as http_error:
		print(f'HTTP error occurred: {http_error}')
	except Exception as err:
		print(f'Other error occurred: {err}')
	else:
		data = json.loads(response.content) 
		d = json.dumps(data, indent=4)
		f.write(d)

def ipVoid(ip):
	print('Checking the Reputation of '+ str(ip))
	baseUrl = 'https://endpoint.apivoid.com/iprep/v1/pay-as-you-go/?key='+str(ipVoidAPI)
	apiUrl = baseUrl + '&ip=' + str(ip)
	#Check the IP for Reputation
	try:
		response = requests.post(apiUrl)
		response.raise_for_status()
	except HTTPError as http_error:
		print(f'HTTP error occurred: {http_error}')
	except Exception as err:
		print(f'Other error occurred: {err}')
	else:
		data = json.loads(response.content) 
		d = json.dumps(data, indent=4)
		f.write(d)

#Main 
if __name__ == "__main__":
	#Arguments to the script
	parser = argparse.ArgumentParser()
	parser.add_argument('-i','--input',dest='fileName',required=True, help='Input File containing list of IPs')
	parser.add_argument('-o','--output',dest='outFile',required=True, help='Provide an File Name to store the results')
	parser.add_argument('-v','--ipvoid',dest='ipvoidBool',action='store_true',required=False, help='Get IP reputation from ipvoid.com')
	parser.add_argument('-q','--ipquality',dest='ipqualityBool',action='store_true',required=False, help='Get IP Reputation from ipqualityscore.com')
		
	#Set args as variables
	args = parser.parse_args()
	fileName = args.fileName
	ipVoidAPI = config.ipVoidAPI
	ipQualityAPI = config.ipQualityAPI
	outFile = args.outFile
	ipvoidBool = args.ipvoidBool
	ipqualityBool = args.ipqualityBool

	#Create array which contains a list of IPs from Input File	
	ipList = [line.rstrip('\n') for line in open(fileName,)]
	#Prepare the outFile
	f = open(outFile,'w+')
	
	for i in range(len(ipList)):
		ip = ipList[i]
		if ipqualityBool:
			print('Using ipqualityscore.com')
			ipQualityScore(ip)
		elif ipvoidBool:
			print('Using ipvoid.com')
			ipVoid(ip)
		else:
			print('Using ipvoid.com')
			ipVoid(ip)
	f.close()
