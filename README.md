# ip-reputation
Check the reputation of a set if IP addresses and return json objects for analysis

# Usage

API Keys are stored in a config.py file stored in the same directory

```python3
ipVoidAPI = 'Your_API_ Key'
ipQualityAPI = 'Your_API_Key'
```

```python3

python3 ipRep.py -i inputFile.txt -o outputFile.txt 

-i , --input		Provide an input file containing a list of IPs to be checked. Each IP should be on its own line.
-o , --output		Provide an output file which will store the resutls in json format
-v , --ipvoid		Use ipvoid API to check the reputation of IPs. This is the deault if no options are given
-q , --ipquality	Use ipquality.com API to check the reputation of IPs.
 
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[GNU](https://choosealicense.com/licenses/agpi-3.0/)
